# Python 3D scene editor

This application is used to construct 3D scenes to be used in the Python 3D Engine.

![Screen](docs/screen.png?raw=true "Screen")

## How to run

### Ubuntu 

- sudo apt-get install python-numpy python-opencv
- sudo apt-get install pyqt4 pyqt-tools pyqt4-dev-tools python-qt4 python-qt4-gl python-pyqt4
- sudo apt-get install qt4-dev-tools libqt4-dev libqt4-core libqt4-gui libqt4-opengl
- sudo apt-get install python-opengl
- sudo pip install pyosc
- Install pyforms: github.com/UmSenhorQualquer/pyforms
- Download the py3DEngine files.
- Unzip the file.
- In the terminal run: python main.py


### Windows

- Install GLUT or Free GLUT: 
    - https://www.opengl.org/resources/libraries/glut/
    - http://freeglut.sourceforge.net/
- Install OpenCV for python
- sudo pip install pyosc
- Install pyforms: github.com/UmSenhorQualquer/pyforms
- Download the py3DEngine files.
- Unzip the file.
- In the terminal run: python main.py