from pyforms import BaseWidget
from pyforms.Controls import ControlButton
from pyforms.Controls import ControlOpenGL
from pyforms.Controls import ControlSlider
from pyforms.Controls import ControlText
from pyforms.Controls import ControlList
from pyforms.Controls import ControlCombo
from pyforms.Controls import ControlFile
from pyforms.Controls import ControlDockWidget
from pyforms.Controls import ControlToolBox
from pyforms.Controls import ControlPlayer
from pyforms.Controls import ControlCheckBox
import pyforms as app

