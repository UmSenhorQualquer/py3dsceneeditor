__version__     = "0.0"
__author__      = "Ricardo Ribeiro"
__credits__     = ["Ricardo Ribeiro"]
__license__     = "Attribution-NonCommercial-ShareAlike 4.0 International"
__maintainer__  = "Ricardo Ribeiro"
__email__       = "ricardojvr@gmail.com"
__status__      = "Development"

from pyforms import BaseWidget
from pyforms.Controls import ControlButton
from pyforms.Controls import ControlOpenGL
from pyforms.Controls import ControlSlider
from pyforms.Controls import ControlText
from pyforms.Controls import ControlTextArea
from pyforms.Controls import ControlList
from pyforms.Controls import ControlCheckBox
from pyforms.Controls import ControlCombo
from pyforms.Controls import ControlFile
from pyforms.Controls import ControlDockWidget
from pyforms.Controls import ControlToolBox
from pyforms.Controls import ControlPlayer
import pyforms as app

